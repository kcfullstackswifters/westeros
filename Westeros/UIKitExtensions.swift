//
//  UIKitExtensions.swift
//  Westeros
//
//  Created by Fernando Rodriguez on 6/28/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import UIKit

extension UIViewController{
    func wrappedInNavigation()->UIViewController{
        let nav = UINavigationController(rootViewController: self)
        return nav
    }
}
